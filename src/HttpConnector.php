<?php

namespace s2AuthComponent;

class HttpConnector {
    private $handle = null;

    public function __construct() {
        $this->handle = curl_init();
    }

    public function setOption($name, $value) {
        curl_setopt($this->handle, $name, $value);
    }

    public function setOptions($data) {
        curl_setopt_array($this->handle, $data);
    }

    public function execute() {
        return curl_exec($this->handle);
    }

    public function getInfo() {
        return curl_getinfo($this->handle);
    }

    public function getError() {
        return curl_errno($this->handle);
    }
    public function getErrorMessage() {
        return curl_error($this->handle);
    }

    public function close() {
        curl_close($this->handle);
    }
}