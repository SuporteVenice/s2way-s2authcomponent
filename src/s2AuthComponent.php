<?php

namespace s2AuthComponent;

use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Exception;
use s2AuthComponent\HttpConnector;

require_once('Constants.php');

class s2AuthComponent {

    private $expSpan = 60*60;
    private $httpConnector;

//     private $pubkey = '
// -----BEGIN PUBLIC KEY-----\n
// MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0FsNr9TgnleMFJxQ/8ia\n
// WoxCalPVpt5RiVajoSaV38IH3+P1DU0uhRIAZ4kll0xWXTM3SvQAq64zSNnvlnSx\n
// gMETu12NaFcAZjfG2Tp9Yu486D0xeEV6QJ7ukoUjdPNGFEJUb2FElEGyzt2VV4KB\n
// WYTe3t90SwxM9v0T8xwDrRUC2SohOPLj0O78MuX0JUseOUyxO2lN+xzmJQsCgZlI\n
// DZZbeHyizlo5vW4rNiSu/Yav/fHGm6+tH1iPrsO3XYdxJF0UMfuNZduhHsWk1Ezu\n
// NsuyWPNSgLx/cZu54Ykg5/Wt6Hv0EVrFfgEnFLz8GrwPW7ilzixFdTM+F4Qh0khh\n
// NwIDAQAB\n
// -----END PUBLIC KEY-----';

    public function __construct() {
    }

    public function setupHttpConnector($connector = null){
        if ($connector == null) {
            $this->httpConnector = new HttpConnector();
        } else {
            $this->httpConnector = $connector;
        }
    }

    public function authenticate($credentials) {

        $URL = AUTH_URL . "/api/authenticate";

        $headers[] = "Authorization: Basic $credentials";
        $headers[] = "Content-type: text/plain";
        $headers[] = "Content-length: 0";

        $options = Array(
            CURLOPT_URL => $URL,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_TIMEOUT => 60, // Em segundos
        );

        $this->httpConnector->setOptions($options);
        $response = $this->httpConnector->execute();
        $info = $this->httpConnector->getInfo();
        $error = $this->httpConnector->getError();
        $errorMessage = $this->httpConnector->getErrorMessage();
        $this->httpConnector->close();

        if ($error) {

            throw new Exception($errorMessage);
        }

        if ($info['http_code'] != 200) {
            throw new Exception($response);
        }

        // Remove double quotes response
        return str_replace('"', '', $response);
    }

    public function validatesS2AuthToken($payToken = '') {

        // Parses from a string
        $decoded = (new Parser())->parse((string) $payToken);

        $now = time();
        $exp = $decoded->getClaim('exp');
        $difference = ($exp - $now);

        // Validade if is necessary refresh token
        if (($exp - $now) < $this->expSpan) {
            return false;
        }

        return true;
    }

    public function extractField($jwt, $field) {

        if (empty($jwt) || empty($field)) {
            return null;
        }

        $decoded = (new Parser())->parse((string) $jwt);

        $extractedField = $decoded->getClaim($field);

        return $extractedField;
    }

    // public function refreshToken($token) {
    //     $this->URL = trim("https://us-central1-desenv-8f439.cloudfunctions.net/refreshToken?token=$token");

    //     $options = Array(
    //         CURLOPT_URL => $this->URL,
    //         CURLOPT_RETURNTRANSFER => true,
    //         CURLOPT_HEADER => false,
    //         CURLOPT_SSL_VERIFYPEER => false,
    //         CURLOPT_TIMEOUT => 60, // Em segundos
    //     );

    //     $curl = curl_init();
    //     curl_setopt_array($curl, $options);

    //     $response = curl_exec($curl);
    //     $info = curl_getinfo($curl);
    //     $error = curl_errno($curl);
    //     $errorMessage = curl_error($curl);

    //     if ($error) {

    //         throw new Exception($errorMessage);
    //     }

    //     // $this->setStatus($info['http_code']);
    //     // $this->setResponse($response);
    //     curl_close($curl);
    //     return $response;
    // }
}
